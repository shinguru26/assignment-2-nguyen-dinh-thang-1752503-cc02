
public class Paladin extends Knight {

	public Paladin(int baseHp, int wp) {
		super(baseHp, wp);
	}

	public double getCombatScore() {
		int nth = 2, numCurr = 1,numPre = 1,temp = 0;
		int baseHp = this.getBaseHp();
		while (numCurr < baseHp ){
			temp = numCurr;
			numCurr+= numPre;
			numPre = temp;
			nth++;
		}
		if (numCurr == baseHp) return 1000.00 + nth;
		else return baseHp*3.00;
	}
	
}
