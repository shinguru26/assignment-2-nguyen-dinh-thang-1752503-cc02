public class Warrior extends Fighter {

	public Warrior(int baseHp, int wp) {
		super(baseHp, wp);
	}

	public double getCombatScore() {
		if (Utility.isPrime(Battle.GROUND)) {
			return this.getBaseHp()*2.00;
		}
		else {
			if (this.getWp() == 1) return this.getBaseHp()*1.00;
			else return this.getBaseHp()/10.00;
		}
	}
}